
let arr=[10,5,2,7,8,7 ];   // given array
let value=3;               // value to create sub-array
let newArray=[];           // new array to push max value from sub-array
let collection=[];         //  collection variable to store value of sub-array

for(let i=0; i<arr.length; i++){   // loop start
    for(let j=0; j<value; j++){
     collection.push(arr[j+i]);    // storing sub-array inside collection temporarily
    }
    newArray.push(Math.max(...collection));   // storing max value from collection 
    collection=[];                            // initializing collection as default for new itetration 
}
 
// loop end

console.log("output====",newArray.slice(0,newArray.length- (value-1)));  // here is the expected output 