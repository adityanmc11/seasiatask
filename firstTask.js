

let givenArray=[1,2,3,4,5];
let newArray=[];
let product = 1;
let i=0;


function productArray(){
  givenArray.forEach(element =>{
    if(i != givenArray.indexOf(element)){
      product=element*product;
    }
  })
  newArray.push(product);
  product=1;
  i++;
  if(i < givenArray.length){
    productArray();
  }
}
productArray();
console.log("newArray is :",newArray);


//  in another way


//   for(i; i<givenArray.length; i++){
//     for(let j=0 ;j<givenArray.length; j++){
//       if(givenArray[i] != givenArray[j]){
//         product=product*givenArray[j];
//       }
//     }
//     newArray.push(product);
//     product=1;
//   }
// console.log("newArray is :",newArray)
