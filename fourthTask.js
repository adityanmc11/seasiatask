let arr=[1, 2, 9, 4, 5 ];                //Given array
let Fsum=arr[0];                         //first sum 
let Ssum=arr[1];                          //second sum
 let Maxadd=0;                            //maximum addition value 
 let Tsum=arr[0]+arr[arr.length-1];       //sum of first and last index of array
maxSum();                                 //function call

// function declear here
function maxSum(){
  
  if(arr.length<=2){        // if loop to check lenth of input is less than 3 or not
    
  return console.log(Math.max(arr[0],arr[arr.length-1]));    // return max value comparing 1st and last index of an array
}

for(let i=0;i<arr.length;i++){    // for loop is initiated

     Fsum+=(arr[i+2])?arr[i+2]:0;   //adding value of index i+2  with Fsum 
     i=i+1;                         // increse value of i with i+1
     Ssum+=(arr[i+2])?arr[i+2]:0;     //adding value of index i+2  with Ssum 

}        // loop is terminated 

  
  if(arr.length>4){        // check length if greater than 4 we proceed futher 
    arr.shift();           // because we are taking arr[0] and arr[arr.length-1]
    arr.pop()              // same as above explanation 
    
    for(let j=0; j<arr.length-1; j++){   // now for loop is initiated 
       Tsum+=(arr[j+1])?arr[j+1]:0;      // adding the value of index j+1 with Tsum
       j=j+2;                             
    }                                     // loop is terminated 
  }
 
    Maxadd=Math.max(Fsum,Ssum,Tsum)      // used math.max function it return the max value 
    return console.log("Max sum of non-adjacent number is :",Maxadd)   // eventually it return max value 
}





